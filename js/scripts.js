$(document).ready(function(){
  	function swipperInit() {
        var wrap = $('.products');

        new Swiper(wrap, {
            slidesPerView: 'auto',
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev' },

            effect: 'coverflow',
            centeredSlides: true,
            coverflowEffect: {
                rotate: 0,
                stretch: -300,
                depth: 1000,
                modifier: 1,
                slideShadows: false },

            loop: true,
            speed: 400,
            observer: true,
            observeParents: true,

            preloadImages: false,
            lazy: {
                loadPrevNext: true,
                loadPrevNextAmount: 3
            },

            breakpoints: {
                1000: {
                    slidesPerView: 1,
                    centeredSlides: false,
                    coverflowEffect: {
                    modifier: 0 },

                spaceBetween: 30 } } 
        });
  	}
    function swipperInitEdge() {
        var wrap = $('.products');

        new Swiper(wrap, {
            slidesPerView: 'auto',
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev' },

            centeredSlides: true,

            loop: true,
            speed: 400,
            observer: true,
            observeParents: true,

            preloadImages: false,
            lazy: {
                loadPrevNext: true,
                loadPrevNextAmount: 3
            },

            breakpoints: {
                1000: {
                    slidesPerView: 1,
                    centeredSlides: false,

                spaceBetween: 30 } } 
        });
    }
    if (/Edge/.test(navigator.userAgent)) {
        swipperInitEdge();
    } else {
        swipperInit();
    }
	$('.feedback-select').select2({
		placeholder: "Выберите тему сообщения",
	    minimumResultsForSearch: Infinity
	});
	$('.burger').click(function(){
		$(this).toggleClass('active');
		$('.menu').toggleClass('active');
		$('body').on('click', function (e) {
	      	var div = $('.menu, .burger');

	      	if (!div.is(e.target) && div.has(e.target).length === 0) {
	        	div.removeClass('active');
	        	$('.burger').removeClass('active');
	      	}
	    });
	});
	$('nav a[href^="#"]').click(function () {
        if($(window).innerWidth() <= 1000) {
           $('.menu').removeClass('active'); 
           $('.burger').removeClass('active'); 
        }
        elementClick = $(this).attr("href");
        if($(window).innerWidth() <= 1000) {
        	destination = $(elementClick).offset().top;
        } else {
        	destination = $(elementClick).offset().top-100;
        }
        $('html').animate( { scrollTop: destination }, 500, 'swing' );
        $('body').animate( { scrollTop: destination }, 500, 'swing' );
        return false;
    });
    $(".phone-mask").inputmask({
	    mask:"+7(999)999-99-99",
	    "clearIncomplete": true
	});
    $('.feedback-form').validate({
        ignore: [],
        errorClass: 'error',
        validClass: 'success',
        rules: {
          	name: {
	            required: true 
	        },
          	email: {
            	required: true,
            	email: true 
            },
          	phone: {
            	required: true,
            	phone: true 
            },
            theme: {
            	required: true
            },
          	messages: {
	            required: true,
	            minlength: 5,
	            normalizer: function normalizer(value) {
	              	return $.trim(value);
	            } 
	        } 
	    },
	    errorElement : 'span',
    	errorPlacement: function(error, element) {
          	var placement = $(element).data('error');
          	if (placement) {
            	$(placement).append(error);
          	} else {
        		error.insertBefore(element);
          	}
        },
        messages: {
          	phone: 'Некорректный номер',
          	email: 'Некорректный e-mail'
      	} 
    });
  	jQuery.validator.addMethod('email', function (value, element) {
    	return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
  	});
  	jQuery.validator.addMethod('phone', function (value, element) {
    	return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
  	});
  	$('.feedback-form .btn').click(function(e){
  		e.preventDefault();
  		if($('.feedback-form').valid()) {
  			//ajax
  			console.log('Сообщение отправлено');
            $('.popup-wrapper').fadeIn('slow');
            $('.popup').fadeIn('slow');
            $('.popup-wrapper, .close-popup').click(function(e){
                $('.popup-wrapper').fadeOut('slow');
                $('.popup').fadeOut('slow');
            });
  		}
  	});
});